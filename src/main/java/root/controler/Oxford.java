package root.controler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.List;
import root.controler.datos;

/**
 *
 * @author Administrador
 */
public class Oxford {
  private String id;
  Oxford MetadataObject;
  //ArrayList<datos> results = new ArrayList<datos>();
 List<datos> results =  new ArrayList<datos>();
  private String word;


 // Getter Methods 

  public String getId() {
    return id;
  }

  public Oxford getMetadata() {
    return MetadataObject;
  }

  public String getWord() {
    return word;
  }

 // Setter Methods 

  public void setId( String id ) {
    this.id = id;
  }

  public void setMetadata( Oxford metadataObject ) {
    this.MetadataObject = metadataObject;
  }

  public void setWord( String word ) {
    this.word = word;
  }

  private String operation;
  private String provider;
  private String schema;


 // Getter Methods 

  public String getOperation() {
    return operation;
  }

  public String getProvider() {
    return provider;
  }

  public String getSchema() {
    return schema;
  }

 // Setter Methods 

  public void setOperation( String operation ) {
    this.operation = operation;
  }

  public void setProvider( String provider ) {
    this.provider = provider;
  }

  public void setSchema( String schema ) {
    this.schema = schema;
  }
}