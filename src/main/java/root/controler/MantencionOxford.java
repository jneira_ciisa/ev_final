/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controler;

import root.controler.Oxford;
import com.dao.TblHistorialJpaController;
import com.dao.exceptions.NonexistentEntityException;
import com.entities.TblHistorial;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author IMG_W10
 */
@WebServlet(name = "MantencionOxford", urlPatterns = {"/MantencionOxford"})
public class MantencionOxford extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MantencionEmpleado</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MantencionEmpleado at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String palabra = request.getParameter("palabra");
        System.out.println(palabra);
        //String palabra = "arbol";
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/"+palabra+"?fields=definitions");
        Invocation.Builder builder = target.request();
        String response2 = builder.header("app_id", "70e31e61")
                .header("app_key", "2bf136de6ae296073d9be6ad1ef65eb1")
                .get(String.class);

        String string = response2;
        String[] parts = string.split("\"definitions\": ");
        String part2 = parts[1]; // 654321
        String string2 = part2;
        String[] parts3 = string2.split("\"");
        String definicion = parts3[1]; // 123
        System.out.println(definicion);
        // Oxford datos =target.request(MediaType.APPLICATION_JSON).get(Oxford.class);
        client.close();
        TblHistorialJpaController dao = new TblHistorialJpaController();

        TblHistorial nuevoTblHistorial = new TblHistorial();
        nuevoTblHistorial.setPalabra(palabra);
        nuevoTblHistorial.setDefinicion(definicion);

        try {
            dao.create(nuevoTblHistorial);
        } catch (Exception ex) {
            System.out.println("Error al crear" + palabra);
            Logger.getLogger(MantencionOxford.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<TblHistorial> TblHistorial = dao.findTblHistorialEntities();

        request.setAttribute("TblHistorial", TblHistorial);
        request.setAttribute("palabra", palabra);
        request.setAttribute("definicion", definicion);
        request.getRequestDispatcher("listado.jsp").forward(request, response);


        
        
        //Client client = ClientBuilder.newClient();
       // WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/cancion?fields=definitions");
       // Oxford datos =myResource.request(MediaType.APPLICATION_JSON).get(Oxford.class);
        
       // request.header("app_id", "70e31e61")
        
     //   .header("app_id", "70e31e61")
       // .headers("app_key", "2bf136de6ae296073d9be6ad1ef65eb1");

         //Oxford datos =myResource.request(MediaType.APPLICATION_JSON).get(Oxford.class);
         //System.out.print("datos.getWord()" + datos.getWord());
         //System.out.print("datos.results.size()" + datos.results.size());
       // request.getRequestDispatcher("indicadores.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
