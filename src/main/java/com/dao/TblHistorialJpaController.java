/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.dao.exceptions.NonexistentEntityException;
import com.entities.TblHistorial;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Administrador
 */
public class TblHistorialJpaController implements Serializable {
private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    public TblHistorialJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    public TblHistorialJpaController() {

    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblHistorial tblHistorial) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(tblHistorial);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblHistorial tblHistorial) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            tblHistorial = em.merge(tblHistorial);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = tblHistorial.getId();
                if (findTblHistorial(id) == null) {
                    throw new NonexistentEntityException("The tblHistorial with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblHistorial tblHistorial;
            try {
                tblHistorial = em.getReference(TblHistorial.class, id);
                tblHistorial.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblHistorial with id " + id + " no longer exists.", enfe);
            }
            em.remove(tblHistorial);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblHistorial> findTblHistorialEntities() {
        return findTblHistorialEntities(true, -1, -1);
    }

    public List<TblHistorial> findTblHistorialEntities(int maxResults, int firstResult) {
        return findTblHistorialEntities(false, maxResults, firstResult);
    }

    private List<TblHistorial> findTblHistorialEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblHistorial.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblHistorial findTblHistorial(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblHistorial.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblHistorialCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblHistorial> rt = cq.from(TblHistorial.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
